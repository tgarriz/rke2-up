# Descripción

El objetivo final es hacer un deploy de un Rancher 2.6.3 con HA, para eso se
recomienda un cluster kubernetes rke2 en el que todos los nodos cumplen con 
los roles ControlPlane, etc y master.

# Para nodos basados en debian

# Requerimiento

Una url balanceada a los nodos del cluster, si no se tiene, apuntar esta url al nodo inicial

# Variables a definir en los playbooks

En set-nodo-inicial
-urlcluster: es la url balanceada o bien que apunta al nodo inicial

En add-nodo.yaml
-urlcluster: es la url balanceada o bien que apunta al nodo inicial
-token: es el token de union al cluster generado en el nodo inicial, se puede obtener 
desde el file /var/lib/rancher/rke2/server/node-token

# Implementacion

Aplicar primero set-nodo-inicial, y luego ir agregando los nodos con add-nodo

